import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "phone"
})
export class PhonePipe implements PipeTransform {
  transform(item): any {
    return item[0] + "*".repeat(item.length - 4) + item.slice(-3);
  }
}
