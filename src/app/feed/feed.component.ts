import { map, takeWhile, tap, startWith, filter } from "rxjs/operators";
import { Router } from "@angular/router";
import { FeedService } from "./../services/feed.service";
import { Feed } from "./../classes/feed";
import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  Subscription,
  BehaviorSubject,
  timer,
  NEVER,
  fromEvent,
  Observable,
  of
} from "rxjs";
import { CustomerService } from "../services/customer.service";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-feed",
  templateUrl: "./feed.component.html",
  styleUrls: ["./feed.component.css"]
})
export class FeedComponent implements OnInit, OnDestroy {
  public feeds: any = [];
  public end: any;
  public currentRoute: string;

  public completedFeeds: any = [];
  private feedSubscription: Subscription;

  constructor(
    private _feedService: FeedService,
    private _customerService: CustomerService,
    private _router: Router
  ) {
    this.feedSubscription = _feedService
      .getCustomers()
      .subscribe((feed: any) => {
        //  this.feeds.unshift(feed);
        this.feeds = feed.customers;
        console.log(feed);
        // this._customerService.getCustomers().subscribe(customers => {
        //   this.feeds = customers;
        //   console.log(this.feeds);
        // });
      });
  }

  notify = (i, phone, ev) => {
    if (!this.completedFeeds.includes(phone)) {
      this._feedService.deActivate(phone).subscribe(resp => {
        this.completedFeeds.push(phone);
        // this._customerService.getCustomers().subscribe(customers => {
        //   this.feeds = customers;
        //   console.log(this.feeds);
        // });
      });
    }
  };

  pause = () => {
    alert("Paused");
  };

  ngOnInit() {
    this.currentRoute = this._router.url;
    this._customerService.getCustomers().subscribe(customers => {
      this.feeds = customers;
      console.log(this.feeds);
    });

    const toggle$ = new BehaviorSubject(true);

    const K = 1000;
    const INTERVAL = K;
    const MINUTES = 1;
    const TIME = MINUTES * K * 60;

    let current: number;
    let time = TIME;

    const toMinutesDisplay = (ms: number) => Math.floor(ms / K / 60);
    const toSecondsDisplay = (ms: number) => Math.floor(ms / K) % 60;

    const toSecondsDisplayString = (ms: number) => {
      const seconds = toSecondsDisplay(ms);
      return seconds < 10 ? `0${seconds}` : seconds.toString();
    };

    const currentSeconds = () => time / INTERVAL;
    const toMs = (t: number) => t * INTERVAL;
    const toRemainingSeconds = (t: number) => currentSeconds() - t;

    const remainingSeconds$ = toggle$.pipe(
      switchMap((running: boolean) => (running ? timer(0, INTERVAL) : NEVER)),
      map(toRemainingSeconds),
      takeWhile(t => t >= 0)
    );

    const ms$ = remainingSeconds$.pipe(
      map(toMs),
      tap(t => (current = t))
    );

    const minutes$ = ms$.pipe(
      map(toMinutesDisplay),
      map(s => s.toString()),
      startWith(toMinutesDisplay(time).toString())
    );

    const seconds$ = ms$.pipe(
      map(toSecondsDisplayString),
      startWith(toSecondsDisplayString(time).toString())
    );

    // update DOM
    const minutesElement = document.querySelector(".minutes");
    const secondsElement = document.querySelector(".seconds");
    const toggleElement = document.querySelector(".timer");

    updateDom(minutes$, minutesElement);
    updateDom(seconds$, secondsElement);

    // set click events
    fromEvent(toggleElement, "click").subscribe(() => {
      toggle$.next(!toggle$.value);
    });

    // update current time on clicks
    toggle$.pipe(filter((toggled: boolean) => !toggled)).subscribe(() => {
      time = current;
    });

    remainingSeconds$.subscribe({
      complete: () => updateDom(of("Take a break!"), toggleElement)
    });

    function updateDom(source$: Observable<string>, element: Element) {
      source$.subscribe(value => (element.innerHTML = value));
    }
  }
  ngOnDestroy() {
    this.feedSubscription.unsubscribe();
  }
}
