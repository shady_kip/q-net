import { BackendInterceptor } from "./services/BackendInterceptor";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./auth/login/login.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { ProfileComponent } from "./auth/profile/profile.component";
import { HomeComponent } from "./home/home.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { LostComponent } from "./lost/lost.component";
import { LandingComponent } from "./landing/landing.component";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ComposerComponent } from "./composer/composer.component";
import { FooterComponent } from "./footer/footer.component";
import { FeedComponent } from "./feed/feed.component";

import { CountdownModule } from "ngx-countdown";
import { CountdownTimerModule } from "ngx-countdown-timer";
import { QuesComponent } from './ques/ques.component';
import { PhonePipe } from './pipes/phone.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    HomeComponent,
    NavbarComponent,
    SidebarComponent,
    LostComponent,
    LandingComponent,
    ComposerComponent,
    FooterComponent,
    FeedComponent,
    QuesComponent,
    PhonePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // CountdownModule,
    CountdownTimerModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BackendInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
