import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { map } from "rxjs/operators";

import * as M from "materialize-css";
import { CustomerService } from "../services/customer.service";
import { FormGroup, FormControl } from "@angular/forms";
import { interval } from "rxjs";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  user: object;
  public isSending: boolean = false;
  showField: boolean = false;
  constructor(
    public _auth: AuthService,
    private _customerService: CustomerService
  ) {}

  customerForm = new FormGroup({
    name: new FormControl(),
    phone: new FormControl(),
    approx_time: new FormControl(),
    custom: new FormControl()
  });

  createCustomer = () => {
    this.isSending = true;
    let customer = {
      name: this.customerForm.value.name,
      phone: this.customerForm.value.phone,
      approx_time: null
    };
    if (this.showField) {
      customer.approx_time = this.customerForm.value.custom;
    } else {
      customer.approx_time = this.customerForm.value.approx_time;
    }

    console.log(customer);
    //TODO: validate details
    this._customerService.createCustomer(customer).subscribe(customer => {
      console.log(customer);
      this.isSending = false;
      this.customerForm.reset();
    });
  };

  ngOnInit() {
    this.customerForm.get("approx_time").valueChanges.subscribe(val => {
      if (val == "custom") {
        this.showField = true;
      } else {
        this.showField = false;
      }
    });
  }
}
