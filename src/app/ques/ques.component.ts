import { Subscription } from "rxjs";
import { Feed } from "./../classes/feed";
import { CustomerService } from "./../services/customer.service";
import { FeedService } from "./../services/feed.service";
import { Component, OnInit, OnDestroy } from "@angular/core";

@Component({
  selector: "app-ques",
  templateUrl: "./ques.component.html",
  styleUrls: ["./ques.component.css"]
})
export class QuesComponent implements OnInit, OnDestroy {
  public feeds: any = [];
  public end: any;

  public completedFeeds: any = [];
  private feedSubscription: Subscription;
  constructor(
    private _feedService: FeedService,
    private _customerService: CustomerService
  ) {
    this.feedSubscription = _feedService
      .getCustomers()
      .subscribe((feed: Feed) => {
        this.feeds.unshift(feed);
        this._customerService.getCustomers().subscribe(customers => {
          this.feeds = customers;
        });
      });
  }
  notify = (i, phone, ev) => {
    if (!this.end) {
      this.end = true;

      if (!this.completedFeeds.includes(phone)) {
        this._feedService.deActivate(phone).subscribe(resp => {
          // this.feeds = resp;
          this.completedFeeds.push(phone);
          this.end = "";
          //  console.log(resp);
          this.feeds.pop(i);
          this._customerService.getCustomers().subscribe(customers => {
            this.feeds = customers;
            console.log(this.feeds);
          });
        });
      }
    }
  };

  ngOnInit() {
    this._customerService.getCustomers().subscribe(customers => {
      this.feeds = customers;
      console.log(this.feeds);
    });
  }

  ngOnDestroy() {
    this.feedSubscription.unsubscribe();
  }
}
