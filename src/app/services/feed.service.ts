import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { Feed } from "../classes/feed";
import Pusher from "pusher-js";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class FeedService {
  private _subject: Subject<Feed> = new Subject<Feed>();
  private _pusherClient: Pusher;

  constructor(private _http: HttpClient) {
    this._pusherClient = new Pusher("3a312fcde602c9402390", { cluster: "ap2" });
    const channel = this._pusherClient.subscribe("my-channel");
    channel.bind("my-event", customers => {
      this._subject.next(customers);
    });
  }

  getCustomers(): Observable<any> {
    return this._subject.asObservable();
  }
  deActivate = phone => {
    return this._http.get("/api/deactivate/customer/" + phone);
  };
}
