import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

export interface TokenPayload {
  email: string;
  password: string;
}
interface TokenResponse {
  token: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private _token: string;
  constructor(private _http: HttpClient, private _router: Router) {}

  public _saveToken(token: string): void {
    localStorage.setItem("usertoken", token);
    this._token = token;
  }

  public _getToken(): string {
    if (!this._token) {
      this._token = localStorage.getItem("usertoken");
    }
    return this._token;
  }
  public _delToken(): boolean {
    localStorage.removeItem("usertoken");

    return true;
  }
  public login(user: TokenPayload): Observable<any> {
    const base = this._http.post(`/api/auth/login`, user);
    const request = base.pipe(
      map((data: TokenResponse) => {
        if (data.token) {
          this._saveToken(data.token);
        }
        return data;
      })
    );

    return request;
  }
  public getUserDetails() {
    const token = this._getToken();
    let payload;
    if (token) {
      payload = token.split(".")[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public getUser(): Observable<any> {
    return this._http.get("/api/user");
  }

  public isLoggedIn(): boolean {
    const user = this.getUserDetails();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  public logout() {
    this._delToken();
    this._http.get("/api/auth/logout");
    this._router.navigateByUrl("/");
  }
}
