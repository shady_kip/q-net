import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
export interface Customer {
  name: string;
  phone: string;
  approx_time: number;
}
@Injectable({
  providedIn: "root"
})
export class CustomerService {
  constructor(private _http: HttpClient) {}
  createCustomer = (customer: Customer): Observable<any> => {
    return this._http.post("/api/customer", customer);
  };
  getCustomers = () => {
    return this._http.get("/api/customers");
  };
}
