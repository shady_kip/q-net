import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";
import {
  HttpInterceptor,
  HttpRequest,
  HttpEvent,
  HttpHandler
} from "@angular/common/http";
import { AuthService } from "./auth.service";

@Injectable()
export class BackendInterceptor implements HttpInterceptor {
  constructor(private injector: Injector, private _authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const cloneReq = req.clone({
      setHeaders: {
        // 'Content-Type' : 'application/json; charset=utf-8',
        Accept: "application/json",
        Authorization: `Bearer ${this._authService._getToken()}`
      }
    });

    return next.handle(cloneReq);
  }
}
