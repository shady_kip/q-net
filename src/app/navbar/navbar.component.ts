import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import * as M from "materialize-css";
import { Router } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  constructor(public _auth: AuthService, private _router: Router) {}
  user: any;
  isLoading: false;

  ngOnInit() {
    this._auth.getUser().subscribe(user => {
      if (!user) {
        this._router.navigateByUrl("/auth/login");
      } else {
        this.user = user;
      }
    });
  }
}
