import { AuthService } from "./../../services/auth.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

import * as M from "materialize-css";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  isLoading: boolean = false;
  constructor(private _auth: AuthService, private _router: Router) {}

  ngOnInit() {
    if (this._auth._getToken()) {
      this._router.navigateByUrl("/home");
    }
  }
  loginForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  login = e => {
    this.isLoading = true;
    let userdata = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };
    this._auth.login(userdata).subscribe(
      data => {
        this.isLoading = false;
        this.loginForm.reset();
        if (data.name) {
          this._auth._saveToken(data.token);
          if (this._auth._getToken) {
            this._router.navigateByUrl("/home");
          } else {
            M.toast({ html: "Error" });
          }
        } else {
          M.toast({ html: data.message });
        }
      },

      err => {
        console.log(err);
      }
    );
  };
}
